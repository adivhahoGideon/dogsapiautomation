package ceo.dog.API;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

import static ceo.dog.API.BaseDogAPI.BASEURL;
import static com.jayway.restassured.RestAssured.given;

public class AllDogsListAPI {

    public AllDogsListAPI(){
        RestAssured.baseURI=BASEURL;
    }

    public Response getListOfAllDogs()throws Throwable{
        return given()
                .contentType(ContentType.JSON)
                .when()
                .get("/breeds/list/all")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .contentType(ContentType.JSON).extract().response();
    }



}
