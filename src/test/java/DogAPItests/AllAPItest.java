package DogAPItests;


import ceo.dog.API.AllDogsListAPI;
import ceo.dog.API.GoldenSubBreedImageAPI;
import ceo.dog.API.RetrieverSubBreedAPI;
import com.jayway.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllDogsListAPI.class,RetrieverSubBreedAPI.class,GoldenSubBreedImageAPI.class})

public class AllAPItest {

    @Autowired
    AllDogsListAPI allDogsListAPIcontroller=new AllDogsListAPI();

    @Autowired
    RetrieverSubBreedAPI retrieverSubBreedController=new RetrieverSubBreedAPI();

    @Autowired
    GoldenSubBreedImageAPI goldenSubBreedImageAPIcontroller=new GoldenSubBreedImageAPI();


    @Test
    public void allDogListAPItest()throws Throwable{

        Response response=allDogsListAPIcontroller.getListOfAllDogs();
        Assert.assertEquals(200,response.getStatusCode());
        response.prettyPrint();

    }


    @Test
    public void allDogListContainsRetrieverTest()throws Throwable{
        Response response=allDogsListAPIcontroller.getListOfAllDogs();
        Assert.assertTrue(response.asString().contains("retriever"));
        System.out.println("Retriever is at index :"+response.asString().indexOf("retriever"));
        response.prettyPrint();
    }

    @Test
    public void retrieverSubBreedAPITest()throws Throwable{
        Response response=retrieverSubBreedController.getRetrieverSubBreeds();
        Assert.assertEquals(200,response.getStatusCode());
        response.prettyPrint();
    }

    @Test
    public void goldenSubBreedImageApiTest()throws Throwable{
        Response response=goldenSubBreedImageAPIcontroller.getRandomGoldenSubBreedImage();
        Assert.assertEquals(200,response.getStatusCode());
        response.prettyPrint();
    }


}
